import tensorflow as tf
from tensorflow.keras import models
from tensorflow.keras import layers
import numpy as np
import pandas as pd
import utils
from os import path

def get_model(settings):
    # Input: 28x28x1
    model = tf.keras.Sequential()
    
    model.add(layers.Conv2D(filters=2, kernel_size=3, strides=1, activation='relu', input_shape=settings["input_shape"]))
    # Size: (N - F + 2 * P)/S + 1 = (28 - 3 + 2 * 0) / 1 + 1 = 25
    # 25x25x2
    model.add(layers.Conv2D(filters=4, kernel_size=5, strides=1, activation='relu'))
    # Size: (N - F + 2 * P)/S + 1 = (25 - 5 + 2 * 0) / 1 + 1 = 21
    # 21x21x4

    model.add(layers.MaxPooling2D(pool_size=3, strides=1))
    # Size: (N - F + 2 * P)/S + 1 = (21 - 3 + 2 * 0) / 1 + 1 = 18 / 1 + 1 = 19
    # 19x19x4

    model.add(layers.Conv2D(filters=8, kernel_size=7, strides=1, activation='relu'))
    # Size: (N - F + 2 * P)/S + 1 = (19 - 7 + 2 * 0) / 1 + 1 = 13
    # 13x13x8
    model.add(layers.Conv2D(filters=16, kernel_size=9, strides=1, activation='relu'))
    # Size: (N - F + 2 * P)/S + 1 = (13 - 9 + 2 * 0) / 1 + 1 = 5
    # 5x5x16

    model.add(layers.Flatten())
    
    model.add(layers.Dense(32, activation='relu'))
    model.add(layers.Dense(64, activation='relu'))
    
    model.add(layers.Dense(len(settings["class_names"]), activation='softmax'))
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    # return it
    return model

if __name__ == "__main__":
    identifier = path.join("models", path.splitext(path.basename(__file__))[0])

    # get the settings, data and model
    settings = utils.get_settings()
    (x_train, y_train), (x_validate, y_validate), (x_test, y_test) = utils.load_data()

    # get variable in scope
    model = None
    construct_model = False 

    # check if model already exists and if we want it
    if path.exists(identifier):
        load = input("Load from disk (y/n)? ")
        if load == 'y':
            model = models.load_model(identifier)
        else:
            print("Recomputing model: " + identifier)
            construct_model = True
    else:
        print("No model found on disk: " + identifier)
        construct_model = True

    history = None
    if construct_model:
        model = get_model(settings)
        # run epoch iterations
        history = model.fit(x_train, y_train, validation_data=(x_validate, y_validate), epochs=settings['epochs'], batch_size=settings['batch_size'])
        # save to disk
        model.save(identifier)

    # convert to predictions
    predictions = model.predict(x_test)

    # generate data for output
    eval_train = model.evaluate(x_train, y_train, settings['batch_size'])
    eval_valid = model.evaluate(x_validate, y_validate, settings['batch_size'])
    eval_test = model.evaluate(x_test, y_test, settings['batch_size'])
    evaluation = pd.DataFrame([eval_train, eval_valid, eval_test], 
        index=["Train", "Validation", "Test"],
        columns=["Loss", "Accuracy"])
    winners = np.argmax(predictions, axis=1)
    matrix = tf.math.confusion_matrix(y_test, winners)

    utils.write_data(identifier, model, matrix, evaluation, history)

    # plot something out
    utils.plot_output(predictions, x_test, y_test, 1)
