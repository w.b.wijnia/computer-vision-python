
### Overall goal

In this assignment, you will get hands-on experience in training and validating CNN models, and how to report the performance of the model. It acts as the foundation for the final assignment.

### Software and installation

We will program in Python. We recommend version 3.8 but anything between Python 3.5 - Python 3.8 (inclusive) is fine. We will use Tensorflow 2.0. In this version, Keras is provided as a package for Tensorflow. Keras will greatly facilitate the development and validation of CNNs. Once you have Python installed, you can simply install TensorFlow with this pip command: $pip install tensorflow See also for GPU support in Tensorflow: https://www.tensorflow.org/install/gpu

### Data

The data for this assignment is the Fashion MNIST dataset from Keras. The dataset is already available as part of TensorFlow/Keras, you can import it by: from keras.datasets import fashion_mnist. It contains 60k training and 10k test images. Each image has one out of ten possible class labels: t-shirts/top, trousers/pants, pullover shirt, dress, coat, sandal, shirt, sneaker, bag or ankle boot. Images are greyscale (single channel) of sizes 28x28.

### Running experiments

If your computer has a GPU with CUDA enabled, you can benefit from significant speed-up, especially when training your models. You can make use of computation services such as Google Colab. Google Cloud will also give you some free allowance on sign-in. Microsoft offers free Azure access for students. There are also free cloud GPU services from Kaggle as well. Important: Don’t forget to shut down your notebooks when you have finished running experiments.

### TensorFlow + Keras vs. OpenCV

We mainly use TensorFlow and Keras instead of OpenCV. But OpenCV might still be your go-to option to address some of the choice tasks, such as data augmentation or preprocessing the data of another dataset. In this case, you can create a separate program/script (in C++ or Python) that does the job and saves the output (images). While the integration of OpenCV in Python is straightforward, you don’t need to do this necessarily.

### Tasks

Your tasks are to develop the scripts to:

 - Import the Fashion MNIST dataset from Keras including the data labels. This would import two sets (training set and test set).
 - You will create a third set (validation set) by splitting the training set into two (training set and validation set) for validation purposes. You should use validation set to evaluate the different choices you make when building your CNNs. Keep in mind that the test set will be used at the very final stage and will not be included in the validation step.
 - Create five different CNN architectures that each takes as input a greyscale image of size 28x28x1, and have 10 outputs (one for each class). The models can have 7 to 10 layers, including the input and output layer. You have to develop five alternative models that can differ in layer type (convolution, dropout, pooling, etc.), activation function, number of kernels, kernel size, stride length, learning rate, etc. We keep the batch sizes fixed so choose a number and keep it constant for all three models. You need to vary at least two parameters in your different models (e.g., not three different stride lengths). You will need to motivate why you try a certain architecture so think about your choices. Also, make sure you can pair-wise compare the architectures, so do not change more than one parameter between two networks.
 - Train each of your five models on the training set and validate them on the validation set of Fashion MNIST. Train and validate each of the five models for up to 15 epochs. In each epoch, store your training and validation loss. From the five trained networks, we choose the best architecture based on the validation set.
 - Draw five different training/validation loss per epoch graph for your five CNNs. Here is an example for this graph. (Note that this is completely random graph from the web for you to see the axes and the lines).
 - Test your best-performing and second best-performing CNNs on the test set. You can now use all the original training set (training set + validation set) to train these models since you have already selected your best model setting.
 - Report your findings (see under submission).

### Grading

The maximum score for this assignment is 100 (grade 10). The assignment counts for 10% of the total grade. You can get 70 regular points and max 30 points for chosen tasks. Mandatory tasks:
 - [x] (15) Create five custom NN/CNNs
 - [x] (20) Train and validate five models on Fashion MNIST training set
 - [ ] (5) Test your best two models on Fashion MNIST test set
 - [ ] (30) Reporting (architectures motivated, results correctly presented, discussion of results, etc.)

Choice tasks also include a reporting aspect. In each case, choose one of your three architectures and report the performance both with and without the novel functionality.
 - [x] (10) Include dense connections in the network.
 - [x] (10) Include residual connections in the network.
 - [x] (10) Provide and explain a confusion matrix for the results on the test set of one of the models.
 - [ ] (10) Create and apply a function to decrease the learning rate at a 1/2 of the value every 5 epochs.
 - [ ] (10) Instead of having a fixed validation set, implement k-fold cross-validation.
 - [ ] (20) Create output layers at different parts of the network for additional feedback. Show and explain some outputs of a fully trained network.
 - [x] (10) Perform data augmentation techniques (at least 3).
 - [ ] (30) Download this dataset, pre-process the images so they fit your network, choose the relevant classes and report the test performance on one of your models.

### Todo

 - [ ] A zip of your code (no binaries, no libraries, no data/model weight files, no images)
 - [ ] A report (2-5 pages)
   - [x] for each of the five models
     - [x] a description (which layers, which dimensions, how connected) and motivation
     - [x] a graph with the training/validation loss on the y-axis and epochs on the x-axis
     - [x] a link to your model’s weights (publicly accessible)
   - [x] For the five models, include a table with the train/validation top-1 accuracy.
   - [ ] Discuss your results in terms of your model (e.g. complexity, type of layers, etc.). Make pair-wise comparisons to those models that differ only on a single parameter. Make sure each model is compared at least once.
   - [ ] Discuss the differences between the two models evaluated on the test set in terms of architecture and comparison with validation performance. How can differences be explained?
   - [ ] Mention which choice tasks you implemented. For each task, look at what additional reporting we expect.