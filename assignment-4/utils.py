import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib 
import numpy as np
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical
import os
import json
import seaborn as sns

if os.name == "nt":
    physical_devices = tf.config.list_physical_devices('GPU') 
    tf.config.experimental.set_memory_growth(physical_devices[0], True)

# use the correct visualisation library
matplotlib.use('TkAgg')

def get_settings():
    class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']
    return {
        "class_names": class_names,
        "batch_size": 10,
        "epochs": 15,
        "input_shape": (28, 28, 1),
    }

def split_validation(x_train, y_train, validation_size=10000):
    x_train, x_validate, y_train, y_validate = train_test_split(x_train, y_train, test_size=validation_size)
    return (x_train, y_train), (x_validate, y_validate)

def load_data():
    mnist = tf.keras.datasets.fashion_mnist
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    x_train.astype('float32')
    x_test.astype('float32')
    x_train, x_test = x_train / 255.0, x_test / 255.0

    (x_train, y_train), (x_validate, y_validate) = split_validation(x_train, y_train)

    # https://stackoverflow.com/questions/63279168/valueerror-input-0-of-layer-sequential-is-incompatible-with-the-layer-expect
    x_train = x_train.reshape(-1, 28, 28, 1) 
    x_validate = x_validate.reshape(-1, 28, 28, 1)
    x_test = x_test.reshape(-1, 28, 28, 1)
    
    # Our labels are categorical values rather than numbers
    # y_train = to_categorical(y_train)
    # y_validate = to_categorical(y_validate)
    # y_test = to_categorical(y_test)

    return (x_train, y_train), (x_validate, y_validate), (x_test, y_test)

def plot_image(i, predictions_array, true_label, img):
    predictions_array, true_label, img = predictions_array[i], true_label[i], img[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    plt.imshow(img, cmap=plt.cm.binary)
    predicted_label = np.argmax(predictions_array)
    if predicted_label == true_label:
        color = 'blue'
    else:
        color = 'red'

    settings = get_settings()
    class_names = settings['class_names']
    plt.xlabel("{} {:2.0f}% ({})".format(class_names[predicted_label], 100*np.max(predictions_array), class_names[true_label]), color=color)

def plot_value_array(i, predictions_array, true_label):
    predictions_array, true_label = predictions_array[i], true_label[i]
    plt.grid(False)
    plt.xticks([])
    plt.yticks([])
    thisplot = plt.bar(range(10), predictions_array, color="#777777")
    plt.ylim([0, 1])
    predicted_label = np.argmax(predictions_array)
    thisplot[predicted_label].set_color('red')
    thisplot[true_label].set_color('blue')

def plot_output(predictions, x_test, y_test, i):
    return
    plt.figure(figsize=(6,3))
    plt.subplot(1,2,1)
    plot_image(i, predictions, y_test, x_test)
    plt.subplot(1,2,2)
    plot_value_array(i, predictions, y_test)
    plt.show()

def write_data(identifier, model, matrix, evaluation, history=None):
    # write out summary
    with open(identifier + '-summary.txt','w') as fh:
        model.summary(print_fn=lambda x: fh.write(x + '\n'))

    # write out evaluation
    with open(identifier + '-evaluation.txt','w') as fh:
        fh.write("%s\n" % evaluation)

    # determine the winning label and construct a confusion matrix
    with open(identifier + '-confusion-matrix.txt','w') as fh:
        fh.write("%s\n" % matrix)

    sns.heatmap(matrix, annot=True, fmt="d")
    plt.savefig(identifier + '-confusion-matrix.png', dpi=300)

    # write out plot model
    tf.keras.utils.plot_model(model, show_shapes=True, to_file=(identifier + '-model.png'))

    # write out a los graph
    if history is not None:
        with open(identifier + '-history.json','w') as fh:
            json.dump(history.history, fh)
