import tensorflow as tf
from tensorflow.keras import models
from tensorflow.keras import layers
import numpy as np
import pandas as pd
import utils
from os import path

def get_model(settings):
    # construct the model
    l_input = layers.Input(shape=settings["input_shape"])

    l_1 = layers.Conv2D(filters=4, kernel_size=3, strides=1, activation='relu')(l_input)
    # Size: (N - F + 2 * P)/S + 1 = (28 - 3 + 2 * 0) / 1 + 1 = 25
    # 25x25x4

    l_2 = layers.Conv2D(filters=4, kernel_size=3, strides=1, padding='same', activation='relu')(l_1)
    # Size: (N - F + 2 * P)/S + 1 = (25 - 3 + 2 * 1) / 1 + 1 = 25
    # 25x25x4

    l_3 = layers.Add()([l_1, l_2])
    
    l_4 = layers.Conv2D(filters=4, kernel_size=3, strides=1, padding='same', activation='relu')(l_3)
    # Size: (N - F + 2 * P)/S + 1 = (25 - 3 + 2 * 1) / 1 + 1 = 25
    # 25x25x4
    
    l_5 = layers.Flatten()(l_4)

    l_6 = layers.Dense(16, activation='relu')(l_5)
    l_7 = layers.Dense(16, activation='relu')(l_6)

    l_output_interim = layers.Dense(len(settings["class_names"]), activation='softmax')(l_5)
    l_output_final = layers.Dense(len(settings["class_names"]), activation='softmax')(l_7)

    model = tf.keras.Model(l_input, l_output_final)
    # model = tf.keras.Model(l_input, [l_output_interim, l_output_final])
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    return model

if __name__ == "__main__":
    identifier = path.join("models", path.splitext(path.basename(__file__))[0])

    # get the settings, data and model
    settings = utils.get_settings()
    (x_train, y_train), (x_validate, y_validate), (x_test, y_test) = utils.load_data()

    # get variable in scope
    model = None
    construct_model = False 

    # check if model already exists and if we want it
    if path.exists(identifier):
        load = input("Load from disk (y/n)? ")
        if load == 'y':
            model = models.load_model(identifier)
        else:
            print("Recomputing model: " + identifier)
            construct_model = True
    else:
        print("No model found on disk: " + identifier)
        construct_model = True

    history = None
    if construct_model:
        model = get_model(settings)
        # run epoch iterations
        history = model.fit(x_train, y_train, validation_data=(x_validate, y_validate), epochs=settings['epochs'], batch_size=settings['batch_size'])
        # save to disk
        model.save(identifier)

    # convert to predictions
    predictions = model.predict(x_test)

    # generate data for output
    eval_train = model.evaluate(x_train, y_train, settings['batch_size'])
    eval_valid = model.evaluate(x_validate, y_validate, settings['batch_size'])
    eval_test = model.evaluate(x_test, y_test, settings['batch_size'])
    evaluation = pd.DataFrame([eval_train, eval_valid, eval_test], 
        index=["Train", "Validation", "Test"],
        columns=["Loss", "Accuracy"])
    winners = np.argmax(predictions, axis=1)
    matrix = tf.math.confusion_matrix(y_test, winners)
    
    utils.write_data(identifier, model, matrix, evaluation, history)

    # plot something out
    utils.plot_output(predictions, x_test, y_test, 1)
