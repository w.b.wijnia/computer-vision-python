import tensorflow as tf

from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental import preprocessing
from sklearn.metrics import classification_report, confusion_matrix

import numpy as np
import pandas as pd
import utils

import os.path
from os import path

def get_model(settings):

    model = tf.keras.Sequential()

    # Input: 28x28x1
    model.add(layers.Conv2D(input_shape=(28, 28, 1), filters=12, kernel_size=7, strides=1, activation='relu'))
    # Size: (N - F + 2 * P)/S + 1 = (28 - 5 + 2 * 0) / 1 + 1 = 24
    # NR params: (5*5 + 1)*12 = 312
    # 24x24x12
    model.add(layers.Dropout(rate=0.2))
    # Size: (N - F + 2 * P)/S + 1 = (24 - 2 + 2 * 0) / 2 + 1 = 22 / 2 + 1 = 12
    # 24x24x12

    # Input: 28x28x1
    model.add(layers.Conv2D(input_shape=(28, 28, 1), filters=12, kernel_size=3, strides=1, activation='relu'))
    # Size: (N - F + 2 * P)/S + 1 = (28 - 3 + 2 * 0) / 1 + 1 = 26
    # NR params: (3*3 + 1)*12 = 120
    # 26x26x12
    model.add(layers.Dropout(rate=0.2))
    # Size: (N - F + 2 * P)/S + 1 = (24 - 2 + 2 * 0) / 2 + 1 = 22 / 2 + 1 = 12
    # 26x26x12

    model.add(layers.Flatten())
    model.add(layers.Dense(64, activation='relu'))
    model.add(layers.Dropout(rate=0.2))
    model.add(layers.Dense(64, activation='relu'))

    model.add(layers.Dense(len(settings["class_names"]), activation='softmax'))
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    return model

if __name__ == "__main__":
    identifier = path.join("models", path.splitext(path.basename(__file__))[0])

    # get the settings, data and model
    settings = utils.get_settings()
    (x_train, y_train), (x_validate, y_validate), (x_test, y_test) = utils.load_data()

    # get variable in scope
    model = None
    construct_model = False 

    # check if model already exists and if we want it
    if path.exists(identifier):
        load = input("Load from disk (y/n)? ")
        if load == 'y':
            model = models.load_model(identifier)
        else:
            print("Recomputing model: " + identifier)
            construct_model = True
    else:
        print("No model found on disk: " + identifier)
        construct_model = True

    history = None
    if construct_model:
        model = get_model(settings)
        # run epoch iterations
        history = model.fit(x_train, y_train, validation_data=(x_validate, y_validate), epochs=settings['epochs'], batch_size=settings['batch_size'])
        # save to disk
        model.save(identifier)
        # write summary to disk

    # convert to predictions
    predictions = model.predict(x_test)

    # generate data for output
    eval_train = model.evaluate(x_train, y_train, settings['batch_size'])
    eval_valid = model.evaluate(x_validate, y_validate, settings['batch_size'])
    eval_test = model.evaluate(x_test, y_test, settings['batch_size'])
    evaluation = pd.DataFrame([eval_train, eval_valid, eval_test], 
        index=["Train", "Validation", "Test"],
        columns=["Loss", "Accuracy"])
    winners = np.argmax(predictions, axis=1)
    matrix = tf.math.confusion_matrix(y_test, winners)

    utils.write_data(identifier, model, matrix, evaluation, history)

    # plot something out
    utils.plot_output(predictions, x_test, y_test, 1)



