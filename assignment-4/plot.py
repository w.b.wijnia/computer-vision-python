import json
from glob import glob
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
from os import path

for fn in glob('./models/*.json'):
    with open(fn,'r') as fh:
        data = json.load(fh)
        data = pd.DataFrame(data)
        data.index = range(1,len(data)+1)

        fn = path.splitext(fn)[0].rsplit('-', 1)[0]

        plt.clf()
        plt.ylim(0.1, 0.65)
        sns.lineplot(data=data[['loss', 'val_loss']])
        plt.legend(labels=['Training Loss', 'Validation Loss'])
        plt.savefig(fn + '-loss-graph.png', dpi=300)

        plt.clf()
        plt.ylim(0.75, 1)
        sns.lineplot(data=data[['accuracy', 'val_accuracy']])
        plt.legend(labels=['Training Accuracy', 'Validation Accuracy'])
        plt.savefig(fn + '-accuracy-graph.png', dpi=300)
