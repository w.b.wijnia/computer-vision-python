import tensorflow as tf
from tensorflow.math import confusion_matrix
from pandas import DataFrame
import os

import utils
from data_videos import load as load_data
from model_two_stream import create as create_model

settings = utils.get_settings()
identifier = "./result/model-two_stream"
identifier_img = "./result/model-base-video"
identifier_vid = "./result/model-base-flow"

# Load the data
classes, train, test = load_data(settings)

model, was_loaded = utils.load_model(identifier)

if not was_loaded:
    input_sizes = [s.shape.as_list()[1:] for s in train.element_spec[0]]
    model_img = utils.load_model_required(identifier_img)
    model_vid = utils.load_model_required(identifier_vid)
    model, model_vid = create_model(settings["width_multiplier"], input_sizes[0], input_sizes[1], len(classes), model_img, model_vid)
    history = model.fit(train, epochs=settings['epochs'], batch_size=settings['batch_size'])
    # save to disk
    model.save(identifier)
    model_vid.save(identifier_vid)
    utils.save_history(identifier, history)

print("Making predictions...")
predictions = model.predict(test)
predictions_true = utils.get_dataset_labels(test)
predictions_winners = utils.get_prediction_winners(predictions)
predictions_matrix = confusion_matrix(predictions_true, predictions_winners)

# generate data for output
train_eval = model.evaluate(train, batch_size=settings['batch_size'])
test_eval = model.evaluate(test, batch_size=settings['batch_size'])
evaluation = DataFrame([train_eval, test_eval], index=["Train", "Test"], columns=["Loss", "Accuracy"])

utils.write_data(identifier, model, predictions_matrix, evaluation)
