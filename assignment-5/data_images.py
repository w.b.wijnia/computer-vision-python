import os
import numpy as np
import tensorflow as tf
from glob import glob

import utils

def move(txt, folder):
    with open('./data/images/ImageSplits/' + txt, 'r') as fh:
        for file in map(str.strip, fh):
            label, outfile = file.rsplit('_', 1)
            os.makedirs(os.path.join(folder, label), exist_ok=True)
            os.replace(
                os.path.join('./data/images/JPEGImages', file),
                os.path.join(folder, label, outfile)
            )

def download(force=False):
    downloaded_images = utils.download_file("http://vision.stanford.edu/Datasets/Stanford40_JPEGImages.zip",
        "./data/images/images.zip")
    downloaded_splits = utils.download_file("http://vision.stanford.edu/Datasets/Stanford40_ImageSplits.zip",
        "./data/images/splits.zip")

    if not force and not downloaded_images and not downloaded_splits:
        return

    move('train.txt', './data/images/train')
    move('test.txt', './data/images/test')

def load(settings):
    download()
    folder_train = './data/images/train'
    folder_test = './data/images/test'
    
    classes = sorted(list(set(map(os.path.basename,
        glob(folder_train + '/*') + glob(folder_test + '/*') 
    ))))

    data_train = tf.keras.preprocessing.image_dataset_from_directory(folder_train, class_names=classes,
        labels='inferred', label_mode='int', color_mode='rgb', batch_size=settings['batch_size'], 
        image_size=settings['image_size'], shuffle=settings['shuffle_data'], interpolation='bilinear')

    data_test = tf.keras.preprocessing.image_dataset_from_directory(folder_test, class_names=classes,
        labels='inferred', label_mode='int', color_mode='rgb', batch_size=settings['batch_size'], 
        image_size=settings['image_size'], shuffle=settings['shuffle_data'], interpolation='bilinear')
    
    return classes, data_train, data_test

if __name__ == "__main__":
    download(force=True)
