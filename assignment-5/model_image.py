import tensorflow as tf
from tensorflow.keras.layers import * 

def block_conv_internal(l_input, filters, stride, padding):
    l_conv_1 = DepthwiseConv2D(kernel_size=3, strides=stride, padding=padding)(l_input)
    l_bn_1 = BatchNormalization()(l_conv_1)
    l_re_1 = ReLU(max_value=6)(l_bn_1)
    l_conv_2 = Conv2D(filters=filters, kernel_size=1)(l_re_1)
    l_bn_2 = BatchNormalization()(l_conv_2)
    l_re_2 = ReLU(max_value=6)(l_bn_2)
    return l_re_2

def block_conv(filters=32, stride=1, padding='same'):
    # TODO: Hier een check of stride=1 en if so dan padding same anders valid?
    return lambda l_input: block_conv_internal(l_input, filters, stride, padding)

def create_layers(a, input_shape, name="MobileNet_V1_IMG"):
    # First layer to reduce size
    l_in = Input(shape=input_shape)
    l = Conv2D(filters=int(32 * a), kernel_size=3, strides=2)(l_in)
    l = BatchNormalization()(l)
    l = ReLU(max_value=6)(l)

    # Stack of dw convolutional layers
    l = block_conv(filters=int(32 * a), stride=1)(l)
    l = block_conv(filters=int(64 * a), stride=2)(l)

    l = block_conv(filters=int(128 * a), stride=1)(l)
    l = block_conv(filters=int(128 * a), stride=2)(l)

    l = block_conv(filters=int(256 * a), stride=1)(l)
    l = block_conv(filters=int(256 * a), stride=2)(l)

    l = block_conv(filters=int(512 * a), stride=1)(l)
    l = block_conv(filters=int(512 * a), stride=1)(l)
    l = block_conv(filters=int(512 * a), stride=1)(l)
    l = block_conv(filters=int(512 * a), stride=1)(l)
    l = block_conv(filters=int(512 * a), stride=1)(l)
    l = block_conv(filters=int(512 * a), stride=2)(l)

    l = block_conv(filters=int(1024 * a), stride=2)(l)

    # Last output layers
    l = GlobalAveragePooling2D()(l)
    l_out = Flatten()(l)
    return tf.keras.Model(l_in, l_out, name=name)

# a = width mutiplier
def create(a, input_shape, number_of_classes, base_model):
    print("Creating model")
    if base_model is None:
        base_model = create_layers(a, input_shape, "MobileNet_V1_IMG")

    l_in = Input(name="input", shape=input_shape)
    l = base_model(l_in)
    l = Dense(int(1000 * a))(l)
    l = ReLU(max_value=6)(l)
    l_out = Dense(number_of_classes, name="output", activation='softmax')(l)

    # return it
    model = tf.keras.Model(l_in, l_out)
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    return base_model, model
