import tensorflow as tf
from tensorflow.keras.layers import * 

from model_image import create_layers

# a = width mutiplier
def create(a, input_shape, number_of_classes, base_model):
    print("Creating model")
    if base_model is None:
        base_model = create_layers(a, input_shape, "MobileNet_V1_FLOW")

    l_in = Input(name="input", shape=input_shape)
    l = base_model(l_in)
    l = Dense(int(1000 * a))(l)
    l = ReLU(max_value=6)(l)
    l_out = Dense(number_of_classes, name="output", activation='softmax')(l)

    # return it
    model = tf.keras.Model(l_in, l_out)
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    return base_model, model
