import tensorflow as tf
from tensorflow.keras.layers import * 
from tensorflow.keras import optimizers

from model_image import create_layers

def create_optimizer(identifier):
    optim = optimizers.get(identifier)
    config = optim.get_config()
    config["learning_rate"] /= 10
    return optim.__class__(**config)

# a = width mutiplier
def create(a, input_shape, number_of_classes, base_model):
    print("Creating model")
    l_in = Input(name="input", shape=input_shape)
    l = base_model(l_in)
    l = Dense(int(1000 * a))(l)
    l = ReLU(max_value=6)(l)
    l_out = Dense(number_of_classes, name="output", activation='softmax')(l)

    # return it
    model = tf.keras.Model(l_in, l_out)
    model.compile(optimizer=create_optimizer('adam'), loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    return base_model, model
