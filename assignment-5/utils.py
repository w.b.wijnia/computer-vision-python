# Tensorflow
import tensorflow as tf
from tensorflow.keras.utils import get_file
import sklearn

# Outputting
import json
import matplotlib 
import matplotlib.pyplot as plt
from pandas import DataFrame
import seaborn as sns

# Utils
import os
import numpy as np

if os.name == "nt":
    physical_devices = tf.config.list_physical_devices('GPU') 
    tf.config.experimental.set_memory_growth(physical_devices[0], True)

# use the correct visualisation library
matplotlib.use('TkAgg')

def get_settings():
    return {
        "width_multiplier": 1.0,
        "flow_frames": 10,
        "shuffle_data": True,
        "batch_size": 10,
        "epochs": 15,
        "image_size": (224, 224), # 200x200 is the smallest image dimensions
    }

def split_validation(x_train, y_train, validation_size=10000):
    x_train, x_validate, y_train, y_validate = sklearn.model_selection.train_test_split(x_train, y_train, test_size=validation_size)
    return (x_train, y_train), (x_validate, y_validate)

def download_file(url, file):
    if os.path.exists(file):
        return False
    if not os.path.exists('./data'):
        os.mkdir('./data')
    subdir = os.path.basename(os.path.dirname(file))
    file = os.path.basename(file)
    get_file(file, url, extract=True, cache_dir="./data", cache_subdir=subdir)
    return True

def encode_labels_sparse(classes, data):
    # we can use searchsorted as a kinda quick hack
    return np.searchsorted(classes, data)

def encode_labels_onehot(data):
    # Note: labels need to be sparse encoded first!
    return tf.keras.utils.to_categorical(data)

def get_dataset_labels(dataset):
    return np.concatenate(list(dataset.map(lambda _, l: l).as_numpy_iterator()))

def get_prediction_winners(predictions):
    return np.argmax(predictions, axis=1)

def load_model(identifier, required=False):
    model = None
    loaded = False
    name = os.path.basename(identifier)

    # check if model already exists and if we want it
    if os.path.exists(identifier):
        if required or input("Load {} from disk (y/N)? ".format(name))[0:1].lower() == 'y':
            print("Loading model {} from disk".format(name))
            model = tf.keras.models.load_model(identifier)
            loaded = True
    else:
        print("No model '{}' found on disk".format(name))

    return model, loaded

def load_model_required(identifier):
    model, loaded = load_model(identifier, required=True)
    if not loaded:
        print("Model {} was not loaded!".format(os.path.basename(identifier)))
        print("Please train image model before transfer learning to video data.")
        quit()
    return model

def load_history(identifier):
    if not os.path.exists(identifier + '-history.json'):
        return None
    with open(identifier + '-history.json','r') as fh:
        return json.load(fh)

def save_history(identifier, history):
    history = history.history

    with open(identifier + '-history.json','w') as fh:
        json.dump(history, fh, indent=2)

    # Plot an loss and accuracy graph from the history
    # TODO: I don't think we will have any validation set, so we should remove that.
    history = DataFrame(history)
    history.index = range(1,len(history)+1)

    # TODO: Determine these limits
    plt.ylim(0, 4.0)
    sns.lineplot(data=history[['loss']])
    plt.legend(labels=['Training Loss'])
    write_plot(identifier, 'loss-graph.png')

    # TODO: Determine these limits
    plt.ylim(0, 1.0)
    sns.lineplot(data=history[['accuracy']])
    plt.legend(labels=['Training Accuracy'])
    write_plot(identifier, 'accuracy-graph.png')

def write_plot(identifier, file):
    plt.savefig(identifier + '-' + file, dpi=300)
    plt.clf()

def write_data(identifier, model, matrix, evaluation):
    # Remove any currently plotted data
    plt.clf()

    # write out plot model
    tf.keras.utils.plot_model(model, show_shapes=True, to_file=(identifier + '-model.png'))
    plt.clf()

    # write out summary
    with open(identifier + '-summary.txt','w') as fh:
        model.summary(print_fn=lambda x: fh.write(x + '\n'))

    # write out evaluation
    with open(identifier + '-evaluation.txt','w') as fh:
        fh.write("%s\n" % evaluation)

    # determine the winning label and construct a confusion matrix
    with open(identifier + '-confusion-matrix.txt','w') as fh:
        fh.write("%s\n" % matrix)

    sns.heatmap(matrix, annot=True, fmt="d")
    write_plot(identifier, "confusion-matrix.png")
