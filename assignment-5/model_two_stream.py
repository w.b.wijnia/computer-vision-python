import tensorflow as tf
from tensorflow.keras.layers import * 

# a = width mutiplier
def create(a, input_shape_image, input_shape_video, number_of_classes, base_model_image, base_model_video):
    print("Creating model")
    base_model_image.trainable = False
    base_model_video.trainable = False

    l_in_image = Input(name="input_image", shape=input_shape_image)
    l_out_image = base_model_image(l_in_image, training=False)

    l_in_video = Input(name="input_video", shape=input_shape_video)
    l_out_video = base_model_video(l_in_video, training=False)

    l_in = Concatenate()([l_out_image, l_out_video])
    l = Dense(int(1000 * a))(l_in)
    l = ReLU(max_value=6)(l)
    l_out = Dense(number_of_classes, activation='softmax')(l)

    # return it
    model = tf.keras.Model(inputs=[l_in_image, l_in_video], outputs=l_out)
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    return model, base_model_video
