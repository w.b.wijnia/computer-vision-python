import os
import numpy as np
import tensorflow as tf
import cv2
from glob import glob

import utils

def set_frame_pos(video, framenr):
    i = 0
    r, f = False, None
    while i < framenr:
        r, f = video.read()
        i += 1
    return r, f

def extract(file_in, folder_out, file_out, frame_offset):
    video = cv2.VideoCapture(file_in)
    nr_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT) / 2)

    video = cv2.VideoCapture(file_in)
    _, frame = set_frame_pos(video, max(0, nr_frames - frame_offset))
    frame_prev = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    hsv = np.zeros_like(frame)
    hsv[..., 1] = 0xff

    index = 0
    while index < frame_offset * 2:
        ret, frame_orig = video.read()
        if not ret: 
            print("warning: not sufficient number of frames for", file_in, file=sys.stderr)
            break
        if index == frame_offset:
            cv2.imwrite(file_out, frame_orig)

        frame = cv2.cvtColor(frame_orig, cv2.COLOR_BGR2GRAY)
        flow = cv2.calcOpticalFlowFarneback(frame_prev, frame, None, 0.5, 3, 15, 3, 5, 1.2, 0)

        mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])
        hsv[..., 0] = ang * 180 / np.pi / 2
        hsv[..., 2] = cv2.normalize(mag, None, 0, 0xff, cv2.NORM_MINMAX)

        rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        cv2.imwrite(os.path.join(folder_out, str(index) + '.jpg'), rgb)

        frame_prev = frame
        index += 1

    video.release()

def move(files, folder, frame_offset):
    os.makedirs(folder, exist_ok=True)
    for file in files:
        label, outfile = file.rsplit('_', 1)
        infile = os.path.join('./data/videos/tv_human_interactions_videos', file)
        outfile = os.path.splitext(outfile)[0] + '.jpg'

        os.makedirs(os.path.join(folder, label), exist_ok=True)
        os.makedirs(os.path.join(folder + "_flow", label, outfile[:-4]), exist_ok=True)
        print("Extracting:", file)
        extract(infile, 
            os.path.join(folder + "_flow", label, outfile[:-4]),
            os.path.join(folder, label, outfile)
        )

def download(frame_offset, force=False):
    classes = ['handShake', 'highFive', 'hug', 'kiss']  # we ignore the negative class

    downloaded_videos = utils.download_file("http://www.robots.ox.ac.uk/~alonso/data/tv_human_interactions_videos.tar.gz",
        './data/videos/videos.tar.gz')
    downloaded_readme = utils.download_file("http://www.robots.ox.ac.uk/~alonso/data/readme.txt",
       "./data/videos/readme.txt")
    
    if not force and not downloaded_videos and not downloaded_readme:
        return classes
    
    set_1_indices = [[2,14,15,16,18,19,20,21,24,25,26,27,28,32,40,41,42,43,44,45,46,47,48,49,50],
                    [1,6,7,8,9,10,11,12,13,23,24,25,27,28,29,30,31,32,33,34,35,44,45,47,48],
                    [2,3,4,11,12,15,16,17,18,20,21,27,29,30,31,32,33,34,35,36,42,44,46,49,50],
                    [1,7,8,9,10,11,12,13,14,16,17,18,22,23,24,26,29,31,35,36,38,39,40,41,42]]
    set_2_indices = [[1,3,4,5,6,7,8,9,10,11,12,13,17,22,23,29,30,31,33,34,35,36,37,38,39],
                    [2,3,4,5,14,15,16,17,18,19,20,21,22,26,36,37,38,39,40,41,42,43,46,49,50],
                    [1,5,6,7,8,9,10,13,14,19,22,23,24,25,26,28,37,38,39,40,41,43,45,47,48],
                    [2,3,4,5,6,15,19,20,21,25,27,28,30,32,33,34,37,43,44,45,46,47,48,49,50]]

    set_1 = (f'{classes[c]}_{i:04d}.avi' for c in range(len(classes)) for i in set_1_indices[c])
    set_2 = (f'{classes[c]}_{i:04d}.avi' for c in range(len(classes)) for i in set_2_indices[c])

    move(set_1, './data/videos/test', frame_offset)
    move(set_2, './data/videos/train', frame_offset)
    return classes

def flatten_flow(batch, labels):
    return tf.concat(tf.unstack(batch), axis=2), labels

def image_to_angles(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    image = np.delete(image, 1, 2) # Remove the saturation (always 1) channel
    return tf.convert_to_tensor(image)

def stack_flow_inner(dataset):
    for images, labels in dataset:
        yield tf.concat([image_to_angles(i) for i in images.as_numpy_iterator()], axis=2), int(list(labels.take(1))[0])

def stack_flow(settings, dataset):
    dataset = dataset.unbatch()
    spec = dataset.element_spec
    dataset = dataset.window(settings['flow_frames'])
    shape = spec[0].shape.as_list()
    shape[2] = (shape[2] - 1) * settings["flow_frames"]
    return tf.data.Dataset.from_generator(lambda: stack_flow_inner(dataset), output_signature=(
        tf.TensorSpec(shape=shape, dtype=spec[0].dtype, name=spec[0].name), spec[1]
        ))

def load(settings):
    classes = download(settings["flow_frames"], force=False)
    
    folder_train = './data/videos/train'
    folder_test = './data/videos/test'

    # Obtain unshuffled data
    data_train_img = tf.keras.preprocessing.image_dataset_from_directory(folder_train, class_names=classes,
        labels='inferred', label_mode='int', color_mode='rgb', batch_size=settings['batch_size'], 
        image_size=settings['image_size'], shuffle=False, interpolation='bilinear')

    data_test_img = tf.keras.preprocessing.image_dataset_from_directory(folder_test, class_names=classes,
        labels='inferred', label_mode='int', color_mode='rgb', batch_size=settings['batch_size'], 
        image_size=settings['image_size'], shuffle=False, interpolation='bilinear')
    
    data_train_vid = tf.keras.preprocessing.image_dataset_from_directory(folder_train+"_flow", class_names=classes,
        labels='inferred', label_mode='int', color_mode='rgb', 
        image_size=settings['image_size'], shuffle=False, interpolation='bilinear')

    data_test_vid = tf.keras.preprocessing.image_dataset_from_directory(folder_test+"_flow", class_names=classes,
        labels='inferred', label_mode='int', color_mode='rgb',
        image_size=settings['image_size'], shuffle=False, interpolation='bilinear')

    # Merge depth channels
    data_train_vid = stack_flow(settings, data_train_vid)
    data_test_vid = stack_flow(settings, data_test_vid)

    # Zip image and video data together, as they belong together
    data_train = tf.data.Dataset.zip((data_train_img.unbatch(), data_train_vid)).map(lambda a, b: ((a[0], b[0]), a[1]))
    data_test = tf.data.Dataset.zip((data_test_img.unbatch(), data_test_vid)).map(lambda a, b: ((a[0], b[0]), a[1]))

    # Shuffle and batch the data
    data_train = data_train.shuffle(len(glob(folder_train+"/*/*"))).batch(settings['batch_size'])
    data_test = data_test.shuffle(len(glob(folder_test+"/*/*"))).batch(settings['batch_size'])

    return classes, data_train, data_test

if __name__ == "__main__":
    download(utils.get_settings()["flow_frames"], force=True)
