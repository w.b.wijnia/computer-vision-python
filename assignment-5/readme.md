### Overall goal
The aim of this assignment is to classify actions first in still images and then in videos with CNNs. The focus is on transfer learning, with the additional use of optical flow and combination of CNN outputs. There are no constraints in terms of the number of layers or the type of the architecture. Again, we’re not looking for the best performance. Make informed choices and reflect on those. We expect a bit more depth in the report in terms of explaining your choices and results. We will again use TensorFlow with Keras. To get you started you can find the skeleton code here that downloads the two datasets and creates lists for you to use. It also includes visualizations of some random instances. You should click Copy to Drive to create a copy of it and start editing.

### Data 

We will use two datasets: 

 - Stanford 40: An image-based dataset for action recognition. It includes 40 action classes with 180-300 images per class (total of 9532 images). The dataset can be downloaded [here]. Use the suggested train/test split (train_files and test_files variables in the skeleton code). When using a validation set, sample from the training set (10% of the training data with stratification). The test set is only seen once you have determined your hyperparameters and trained the corresponding model. 
 - TV Human Interaction (TV-HI): Includes 4 interaction classes (handshake, high five, kiss and hug) with 300 video clips (1-5 second segments) from TV shows. Additionally, it contains (negative) clips that do not contain any interaction. You can discard these clips. The dataset can be found [here]. Use the provided Set 1 (see readme.txt) for testing and Set 2 for training and validation. Use 10%-15% of Set 2 for validation and use stratification. This doesn’t give you many videos for training and validation, but that's not a problem. We are not using the annotation files, just stick to the images and the splits in the readme.txt which is already prepared for you in the skeleton code.

### Tasks 

 - [ ] Create a CNN and train it on the images in Stanford 40. Naturally, you will have 40 output classes. You are free in developing your own network, without any constraints. Make sure you motivate your choices well. Select your hyperparameters (on the validation set you create with stratification) and report the performance on both the validation and the test set (see under “Submission”). 
 - [ ] Use transfer learning and fine-tune a new CNN (same architecture apart from the output layer) on frames from videos of the TV Human Interaction dataset. You can use the middle frame of each video, for example. For the fine-tuning, use a reduced learning rate of 1/10 of the original value trained on Stanford 40. Again, report the performance with your selected set of hyperparameters. For this task, you do not train from scratch. 
 - [ ] Create a model with the same architecture (apart from the input layer) and train it on the optical flow of videos in TV Human Interaction. You can use the middle frame (max 10 points) or stack a fixed number (e.g., 16) of optical flow frames together (max 15 points). Again, report the performance with your selected set of hyperparameters. 
 - [ ] Finally, create a two-stream CNN with one stream for the frames and one stream for the optical flow. Use your pre-trained CNNs (one for the frames and one for the optical flow). Think about how to connect the two streams and motivate this in your report. Look at the Q&A at the end of this assignment. Fine-tune the network. Report the performance. For this task, you do not train from scratch.

### Submission

You can use infomcv_assignment_5_report_template.docx to write your report.
Through Blackboard, hand in the following two deliverables: 

 - [ ] A zip with your code (no binaries, no libraries, no data/model weight files!)  
 - [ ] A report (5 pages max) with (a) 
   - [ ] a brief description and motivation of your implementation (both frame CNN and optical flow CNN), detailing the architecture and parameter values. (b) 
   - [ ] One table and eight graphs. In the table, include the best rates achieved on each of the datasets, the models chosen and the top-1 accuracy and loss on the train and test sets (see example below). 
   - [ ] Provide train-validation accuracy and train-validation loss graphs for: 
     - [ ] (1-2) your frame CNN on the Stanford 40 dataset, 
     - [ ] (3-4) your transfer learned frame CNN on TV-HI, 
     - [ ] (5-6) your optical flow CNN on TV-HI, and 
     - [ ] (7-8) your two-stream model on TV-HI. In all cases, report only the runs with the selected hyperparameters. Use different colours for different models and different colour shades for train/validation. 
   - [ ] Explain your results in terms of your architecture and the training procedure.  
   - [ ] Add a link to your model weights (in Dropbox or Google/One Drive), and  
   - [ ] clearly mention which choice tasks you implemented. 
  

| Dataset | Model | Top-1 acc. | Top-1 loss | Model size (GB)|
|---------|-------|------------|------------|----------------|
| Stanford40 | Frames | W% | 0.www | w.ww |
| TV-HI | Frames | X% | 0.xxx | x.xx | TV-HI | Optical flow | Y% | 0.yyy | y.yy |
| TV-HI | Two-stream | Z% | 0.zzz | z.zz |

### Grading 

The maximum score for this assignment is 100 (grade 10). The assignment counts for 20% of the total grade. You can get 70 regular points and max 30 points for chosen tasks: 

 - [ ] Create custom CNN and train/validate/test it on Stanford 40: 10
 - [ ] Apply transfer learning and re-train (fine-tune)  your CNN on TV-HI: 10
 - [ ] Calculate the optical flow for TV-HI and train/validate/test a CNN using optical flow on TV-HI: 15 
 - [ ] Train a Two-stream CNN and train/validate/test it on TV-HI: 15 
 - [ ] Reporting: 20 

The options below are eligible for choice points. Check with Metehan if you have other ideas. For each task,  report the change in performance for one model (CNN/optical flow/Two-stream).

 - [ ] Do data augmentation for the frame-CNN: 5
 - [ ] Create a Cyclical Learning rate schedule: 5
 - [ ] Use a kernel regularizer for your convolutions: 5 
 - [ ] Create connections from the optical flow CNN to the frames CNN (before the fusion layers): 10
 - [ ] Use 1x1 Convolutions to connect activations between the two branches: 10
 - [ ] Experiment with additional types of cooperation for the two networks (average, mean, addition etc.): max 15 