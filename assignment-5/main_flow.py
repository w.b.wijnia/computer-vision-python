from tensorflow.math import confusion_matrix
from pandas import DataFrame

import utils
from data_videos import load as load_data
from model_flow import create as create_model

settings = utils.get_settings()
identifier = "./result/model-flow"
identifier_base = "./result/model-base-flow"

# Load the data
classes, train, test = load_data(settings)
train = train.map(lambda i, l: (i[1], l))
test = test.map(lambda i, l: (i[1], l))

# Load the model from disk or fit a new one
model, was_loaded = utils.load_model(identifier)

if not was_loaded:
    input_size = tuple(train.element_spec[0].shape.as_list()[1:])
    model_base, _ = utils.load_model(identifier_base)
    model_base, model = create_model(settings["width_multiplier"], input_size, len(classes), model_base)
    history = model.fit(train, epochs=settings['epochs'], batch_size=settings['batch_size'])
    # save to disk
    model_base.save(identifier_base)
    model.save(identifier)
    utils.save_history(identifier, history)

# convert to predictions
print("Making predictions...")
predictions = model.predict(test)
predictions_true = utils.get_dataset_labels(test)
predictions_winners = utils.get_prediction_winners(predictions)
predictions_matrix = confusion_matrix(predictions_true, predictions_winners)

# generate data for output
train_eval = model.evaluate(train, batch_size=settings['batch_size'])
test_eval = model.evaluate(test, batch_size=settings['batch_size'])
evaluation = DataFrame([train_eval, test_eval], index=["Train", "Test"], columns=["Loss", "Accuracy"])

utils.write_data(identifier, model, predictions_matrix, evaluation)
