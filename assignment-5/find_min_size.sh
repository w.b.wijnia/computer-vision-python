#!/usr/bin/sh

python -c "$( \
	echo -n 'import numpy; print(numpy.array([' && \
	fd . ./data -e jpg -x identify {} | cut -d ' ' -f 3 | sed -E 's/(.*)x(.*)/[\1,\2],/' | tr '\n' ' ' && \
	echo -n ']).min(axis=0));' \
)"
